# web:21debian
# Isaac Gordillo

## Web21:debian servidor apache detach

Lo primero que hay que hacer es crear el archivo Dockerfile. Hay un ejemplo del contenido de este archivo al final de la página.

```
vim Dockerfile
```

Tras esto, hay que crear una imagen del container.

```
docker build -t isaacgm22/web21_buena:debian .
```

Finalmente, se crea un container que se autodestruye (--rm) y que va solo (-d).

```
docker run --rm -d isaacgm22/web21_buena:debian
```

## EXPLICACIÓN DE LO QUE CONTIENE EL DOCKERFILE

- De qué distribución y versión de Linux se va a crear el contenedor. Por ejemplo, 'fedora:27'.

```
FROM linux-distribution:version 
```

- Etiquetas (opcional).

```
LABEL subject="webserver"
LABEL author="name"
```

- Qué utilidades se quieren instalar. Estas son las básicas para hacer una página web.
  '&&' hace que se ejecuten las dos órdenes de forma simultánea

```
RUN apt-get update && apt-get -y install apache2 iputils-ping iproute nmap procps
```

- Copiar el archivo index.html (al final de la página hay un ejemplo), que contiene
  la página web, al directorio /var/www/html.

```
COPY index.html /var/www/html
```

- Hacer que el directorio activo sea /tmp.

```
WORKDIR /tmp
```

- Encender el servicio de apache2. -k y -X hace que vaya en detach.

```
CMD apachectl -k start -X
```

- Abrir el puerto 80, que es el de HTTP.

```
EXPOSE 80
```

## EJEMPLO DE DOCKERFILE

```
# Web server apache2 debian detach

FROM debian
LABEL subject="webserver"
LABEL author="Isaac"
LABEL verdura="bledes"
RUN apt-get update && apt-get -y install apache2 iputils-ping iproute2 nmap procps
COPY index.html /var/www/html
WORKDIR /tmp
CMD apachectl -k start -X
EXPOSE 80
```

## EJEMPLO DE INDEX.HTML

```
<html>
  <head>
    <title>HISX1 M05 WEB</title>
  </head>
  <body>
    <h1> Guide to git gud at Dark Souls 1 </h1>
    <h2> Ornstein & Smough no hit</h2>
    <p>  https://www.youtube.com/watch?v=q077YGj_ssE </p>
  </body>
```
